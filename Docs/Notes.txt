
------------------------- Status Bar -----------------------------

Status bar of QMainWindow is retrieved by statusBar() function. setStatusBar() function activates it.

 - self.statusBar = QStatusBar()
 - self.setStatusBar(self.statusBar)

change message with:
-------------------

 - self.statusBar.showMessage("__")

------------------------- Center Window ---------------------------

Code ********understand later******

>     def center(self):
>         frameGm = self.frameGeometry()
>         screen = QApplication.desktop().screenNumber(QApplication.desktop().cursor().pos())
>         centerPoint = QApplication.desktop().screenGeometry(screen).center()
>         frameGm.moveCenter(centerPoint)
>         self.move(frameGm.topLeft())  

------------------------ Second Window ---------------------------

Dialogs - A dialog is used to input data, modify data, change the application settings etc.
	- QInputDialog a single value from the user. The input value can be a string, a number, or an item from a list. ( Kanji look up)
	- 

Custom dialog:
dialog class initialized as:
---------------------------

> class MyPreferences(QDialog):
>     def __init__(self):
>         '''
>         Initialize the window
>         '''
>         super(MyPreferences, self).__init__()

instanciating class in main window:
----------------------------------
> MyPreferences().exec_()

------------------------- Menu Bar ------------------------------

To create a menu for a PyQt5 program we need to use a QMainWindow. The top menu can be created with the method menuBar(). Sub menus are added with addMenu(name)

ex):
>   ...
>
>   mainMenu = self.menuBar() 
>   fileMenu = mainMenu.addMenu('File')
>
>   ...
>
>   self.show

Adding buttons and actions:
	- before show

>   exitButton = QAction(QIcon('exit24.png'), 'Exit', self)
>   exitButton.setShortcut('Ctrl+Q') 
>   exitButton.setStatusTip('Exit application')
>   exitButton.triggered.connect(self.close)
>   fileMenu.addAction(exitButton)

Calling new window:
------------------

have a function to call new window class and execute it:
        
>   def showPref(self):
>       p = MyPreferences()
>       p.exec_()

QAction calls the function like a member variable:

>   prefButton.triggered.connect(self.showPref)
>   editMenu.addAction(prefButton)

--------------------- Importing (Python)  ------------------------

Klasa ==> *ClassName or MethodName*

>   from folder.file import Klasa

Or, with from folder import file:

>   from folder import file
>   k = file.Klasa()

Or again:

>   import folder.file as myModule
>   k = myModule.Klasa()


---------------------- Table Widget ------------------------------

Hide table index number ( vertical header):
------------------------------------------
> myTable.verticalHeader().setVisible(false)

Hide Grid lines:
---------------
> myTablesetShowGrid(False

Table Header mods:
**********************************

for individual sections:
-----------------------

> myTable.horizontalHeader().setSectionResizeMode(0, QHeaderView.Interactive)
> myTable.horizontalHeader().setSectionResizeMode(1, QHeaderView.Stretch)

for entire header:
------------------

>  myTable.horizontalHeader().setResizeMode(1, QHeaderView.Stretch)

QHeaderView Options:

    QHeaderView.Interactive	
        - The user can resize the section. The section can also be resized programmatically using resizeSection(). The section size defaults to defaultSectionSize. (See also cascadingSectionResizes.)

    QHeaderView.Fixed 
        - The user cannot resize the section. The section can only be resized programmatically using resizeSection(). The section size defaults to defaultSectionSize.

    QHeaderView.Stretch
         - QHeaderView will automatically resize the section to fill the available space. The size cannot be changed by the user or programmatically.
 
    QHeaderView.ResizeToContents
         - QHeaderView will automatically resize the section to its optimal size based on the contents of the entire column or row. The size cannot be changed by the user or programmatically. (This value was introduced in 4.2)

Set minimum column Size:
> myTable.horizontalHeader().setMinimumSectionSize(80)

************************************

------------------------ Layouts ---------------------------------

multilayout window:
------------------

create QWidgets and Qframes and add to QSplitter

1) add desired widgets to a layout then apply that layout to a frame or custom widget. 
2) add the custom widget or fram to a splitter. 
3) add the splitter as a widget in a final layout.
4) set the final layout to a custom widget
5) set custom widget as central widget in mainwindow

widgets -> layouts -> frame/custom widget -> spliter -> layout -> customwidget -> main window

ex)

> layout = QHBoxLayout()
> label = QLabel(self)
> layout.addWidget(label.setText("Hello"))
> 
> #################################
>
> right = QFrame(self)
> right.setFrameShape(QFrame.StyledPanel)
> right.setLayout(layout)
>
> or
>
> right = QWidget()
> right.setLayout(layout)
>
> #################################
>
> split = QSplitter(Qt.Horizontal) 
> split.addWidget(right)
>
> hbox = QHBoxLayout()
> hbox.addWidget(split)
>
> widget = QWidget()
> widget.setLayout(hbox)
>
> QMainWindow.setCentralWidget(widget) 
>

------------------------- splitter -----------------------------

disable collapsible sides:

>    self.split.setChildrenCollapsible(False) #set all collapsible

>    self.split.setCollapsible(1,False) # set just the middle non collapsible

set position splitters:

>    self.split.setStretchFactor(0,1)
>    self.split.setStretchFactor(1,0)
>    self.split.setStretchFactor(2,0)
        
---------------------- TreeView --------------------------------

use abstract model to create the table and functionaliy given the data and header:
-------------------------------

Template
> class MyTableModel(QAbstractTableModel):
>     def __init__(self, list, headers = [], parent = None):
>         QAbstractTableModel.__init__(self, parent)
>         self.list = list
>         self.headers = headers
> 
>     def rowCount(self, parent):
>         return len(self.list)
> 
>     def columnCount(self, parent):
>         return len(self.list[0])
>     
>     def flags(self, index):
> 
>     def data(self, index, role):
> 
>     def setData(self, index, value, role = Qt.EditRole):
> 
>     def headerData(self, section, orientation, role):
> 

calling Abstract model to create table

>        headers = ["000", "001", "002"]
>        tableData0 = [
>                 ['abc',100,200],
>                 ['fff',130,260],
>                 ['jjj',190,300],
>                 ['ppp',700,500],
>                 ['yyy',800,900]
>                 ]
>
>        model = MyTableModel(tableData0, headers)
>        self.dataTable = QTreeView()
>        self.dataTable.setRootIsDecorated(False) #remove expand/collapse indicator
>        self.dataTable.setModel(model)

removeing expande/collapse indicator:
------------------------------------

>        self.dataTable.setRootIsDecorated(False) #remove expand/collapse indicator



----------------------- Form Layout --------------------------

elements can be added individually:

> QFormLayout.addWidget(QWidget)

or in one of the forms below:

> QFormLayout.addRow(QWidget, QWidget)
> QFormLayout.addRow(QWidget, QLayout)
> QFormLayout.addRow(str, QWidget)
> QFormLayout.addRow(str, QLayout)
> QFormLayout.addRow(QWidget)
> QFormLayout.addRow(QLayout)


----------------------- Tabs ----------------------------------


  
> self.centralwidget = QWidget(self)
> self.centralwidget.setObjectName("centralwidget")
>         
> self.tabWidget = QTabWidget(self.centralwidget)
> self.tabWidget.setTabPosition(QTabWidget.West)
> self.tabWidget.setObjectName("tabWidget")
>         
> self.tabWidget.addTab(MainLayout(), "Tab1")
> self.tabWidget.addTab(MainLayout(), "Tab2")
>        
> self.setCentralWidget(self.tabWidget)


------------------------ PyMongo -----------------------------

 You can also specify the host and/or port using:

> client = MongoClient('localhost', 27017)

Or just use the Mongo URI format:

> client = MongoClient('mongodb://localhost:27017')

To specify which database you actually want to use, you can access it as an attribute:

> db = client.pymongo_test

Or you can also use the dictionary-style access:

> db = client['pymongo_test']

Getting a collection in PyMongo works the same as getting a database:

> collection = db.test_collection

or (using dictionary style access):

> collection = db['test-collection']


---------------------- Python --------------------------------

iterate through dictionary:
--------------------------

To loop all the keys from a dictionary – for k in dict:

    for k in dict:
	    print(k)
	    
To loop every key and value from a dictionary – for k, v in dict.items():

    for k, v in dict.items():
	    print(k,v)

P.S items() works in both Python 2 and 3.








