#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import QApplication
from MainWindow import Manager

if __name__ == '__main__':
    
    app = QApplication([])
    manager = Manager()    
    sys.exit(app.exec_())
