import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import * 
from Center import center
from Preferences import *

class Manager(QMainWindow):
    
    def __init__(self):
        super().__init__()
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.initUI()
        self.move(center(self))
        
    def initUI(self):    
        '''
        initiates application UI
		'''
        self.statusBar = QStatusBar()
        self.statusBar.showMessage("Hello")
        self.setStatusBar(self.statusBar)

        self.initMenuBar()
        self.loginUI()
        self.show()
        
    def loginUI(self):
        
        self.setWindowTitle('3rdEye Manager')  
        self.setGeometry(self.left,self.top,self.width, self.height) # set screen size (left, top, width, height
        
        self.idLabel = QLabel(self)
        self.idLabel.setText('ID: ')
        self.idLabel.move(120,200)
        
        self.idBox = QLineEdit(self)
        self.idBox.move(220, 200) # move(width, height)
        self.idBox.resize(200, 32) 
        
        self.passwordLabel = QLabel(self)
        self.passwordLabel.setText('PASSWORD: ')
        self.passwordLabel.move(120,240)
        
        self.passwordBox = QLineEdit(self)       
        self.passwordBox.move(220, 240)
        self.passwordBox.resize(200, 32)
        
        self.submitButton = QPushButton("Enter", self)
        #submitButton.clicked.connect(self.clickMethod)
        self.submitButton.resize(200,32)
        self.submitButton.move(220, 280)   
        
    
    def initMenuBar(self):

        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('&File')
        editMenu = mainMenu.addMenu('&Edit')
        helpMenu = mainMenu.addMenu('&Help')
        
        """
        File Menu
        """
        exitButton = QAction(QIcon('exit24.png'), 'Exit', self)
        exitButton.setShortcut('Ctrl+Q')
        exitButton.setStatusTip('Exit application')
        exitButton.triggered.connect(self.close)
        fileMenu.addAction(exitButton)
        
        """
        Edit Menu
        """
        prefButton = QAction('Preferences', self)
        prefButton.setStatusTip('Exit application')
        prefButton.triggered.connect(self.showPref)
        editMenu.addAction(prefButton)
        
    def showPref(self):
        p = MyPreferences()
        p.exec_()
        
        
