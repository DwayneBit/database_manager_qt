import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import * 
from Center import center

"""
DataTable: 
    table of data from database
"""
class DataTable(QHBoxLayout):

    def __init__(self):
        super().__init__()
        
        self.addWidget(self.createTable())
        
    def createTable(self):
    
        headers #get headers from MongoDB
        tableData #get data from mongoDB

        model = MyTableModel(tableData, headers)
        
        self.dataTable = QTreeView()
        self.dataTable.setRootIsDecorated(False) #remove expand/collapse indicator
        self.dataTable.setModel(model)
        
"""
DataTableModel: 
    Table model for DataTable
"""        
class DataTableModel(QAbstractTableModel):
    def __init__(self, content, headers = [], parent = None):
        QAbstractTableModel.__init__(self, parent)
        self.content = content
        self.headers = headers

    def rowCount(self, parent):
        return len(self.content)

    def columnCount(self, parent):
        return len(self.content[0])
    
    def flags(self, index):

    def data(self, index, role):

    def setData(self, index, value, role = Qt.EditRole):

    def headerData(self, section, orientation, role):
    
"""
MainLayout:
    central Widget of main window contains page UI
"""
class MainLayout(QWidget):

    def __init__(self):
        super().__init__()
        self.hbox = QHBoxLayout()
        self.split = QSplitter(Qt.Horizontal)
        
        self.split.addWidget(self.createTableFrame())
        self.split.addWidget(self.createFormFrame())
 
        #self.split.setCollapsible(1,False) # set just the middle non collapsible
        self.split.setChildrenCollapsible(False) #set all collapsible
        self.split.setStretchFactor(0,1)
        self.split.setStretchFactor(1,0)
        self.split.setStretchFactor(2,0)
        
        self.hbox.addWidget(self.split)
        self.setLayout(self.hbox)
        
    def createTableFrame(self):
        
        tLayout = DataTable(''' Data from individual collection ''')
        
        tFrame = QFrame(self)
        tFrame.setFrameShape(QFrame.StyledPanel)
        tFrame.setLayout(tLayout)
        
        return tFrame
        
        
    def createFormFrame(self):
        layout = QFormLayout()
        
        #i = number of elements in document
        #for ele in i
        #   label = QLabel(self)
        #   label.setText(nameOfColumn)
        #   box = QLineEdit(self)
        
        #   layout.addRow(label,box)
        #                
        
        fFrame = QFrame(self)
        fFrame.setFrameShape(QFrame.StyledPanel)
        fFrame.setLayout(layout)
        
        return fFrame

"""
StatusBar:
"""
class StatusBar(QStatusBar):
    def __init__(self):
        super().__init__()
        self.showMessage("Hello")
    
    
        
class Manager(QMainWindow):
    
    def __init__(self):
        super().__init__()
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.initUI()
        self.move(center(self))
        
    def initUI(self):
    
        self.setWindowTitle('DBManager')  
        self.setGeometry(self.left,self.top,self.width, self.height) # set screen size (left, top, width, height     
        
        self.tabWidget = QTabWidget(self)
        self.tabWidget.setTabPosition(QTabWidget.West)
        self.tabWidget.setObjectName("tabWidget")
        
        #for collection in database
        #   self.tabWidget.addTab(MainLayout(), "NameOfCollection")
        
        self.setCentralWidget(self.tabWidget)
        
        self.initToolBar()
        self.setStatusBar(StatusBar())
        
        self.show()
        
    def initToolBar(self):)
        
        tb = self.addToolBar("File")
        
        #tbopen = QAction(QIcon("internet.png"),"open",self)
        #tb.addAction(tbopen)
        
        
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    manager = Manager()    
    sys.exit(app.exec_())
