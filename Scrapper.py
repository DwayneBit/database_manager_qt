from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import * 
from bs4 import BeautifulSoup
from Preferences import Preferences
from Center import center
from getType import getType
import re
import os.path
import json
import pprint
import requests

class Scrapper:
    def __init__ (self, scrapPrefs, linkKeyList = None ):

        self.scrapPrefDict = scrapPrefs

        if isinstance(linkKeyList, str):
            self.linkKeyList = [linkKeyList]
        else:
            self.linkKeyList = linkKeyList

        self.searches = scrapPrefs["s"]
        self.page_link = ""
        self.results = {}

        self.webPrefs = None

    '''
    createPageLink:
    creates the link to the page with the link frame give in preference
    dictionary and replaces '*' with the key words or characters used
    to get a specific page

    return:
    string with exact link to search
    '''
    def createPageLink(self):
        
        count = 0
        
        for i in range(len(self.scrapPrefDict['page_link'])):
            if self.scrapPrefDict['page_link'][i] == '*' and count < len(self.linkKeyList):
                self.page_link += self.linkKeyList[count]
                count += 1

            else:
                self.page_link += self.scrapPrefDict['page_link'][i]

        #print(self.page_link)

    '''
    getScraps:
    uses the information found give in the preferencse dictionary to
    get multiple eliments from a website. 's#' indicates a new element to 
    find in the web page.
    '''
    def getScraps(self):
        if self.page_link == "":
            self.createPageLink()

        for i in range(len(self.searches)):
            s = 's'+str(i+1)
            if (self.searches[s]["search"]):
                print(s)
                # fetch the content from url
                page_response = requests.get(self.page_link, timeout=5)
                # parse html
                page_content = BeautifulSoup(page_response.content, "html.parser")
                #print(page_content)
                find = page_content.find_all(self.searches[s]['tag'], attrs = { 'class' : self.searches[s]['class'], 'id' : self.searches[s]['id']})

                results = []
                for j in range(len(find)):
                    if j >= self.searches[s]['results'] :
                        #print(self.searches[s]['results'])
                        break
                    else:
                        if self.searches[s]['clean']:
                            clean = self.cleanWhiteSpace(find[j].get_text())
                            if isinstance(clean, list):
                                results += clean
                            else:
                                results.append(clean)

                        else:
                            results.append(find[j].text)
                            #print(find[j].text)

                self.results[s] = results
                print(results)
            #print(self.cleanWhiteSpace(find))

    '''    
    def strip_tags(self, html, invalid_tags):
        html = html.get_text()
        print(html)
        soup = BeautifulSoup(html, "lxml")

        for tag in soup.findAll(True):
            if tag.name in invalid_tags:
                s = ""

                for c in tag.contents:
                    if not isinstance(c, NavigableString):
                        c = strip_tags(unicode(c), invalid_tags)
                    s += unicode(c)

                tag.replaceWith(s)

        return soup
    '''

    '''
    cleanWhiteSpace:
    removes with spaces from string where the word or character may
    be padded
    '''
    def cleanWhiteSpace(self, strg):
        wSpace = set([' ','\t'])
        print(strg)
        strg2 = ""
        lst = []
        for i in range(len(strg)):
            if strg[i] == "\n" and not strg2 == "":
                lst.append(strg2)
                strg2="";
            elif strg[i] not in wSpace and not strg[i] == "\n":
                strg2 += strg[i]
        
        if not strg2 == "":
            lst.append(strg2)

        return lst

    '''
    saveSearch:
    save search so that it may be reused later
    '''
    def saveSearch(self, database, collection, num = ""):

        self.scrapPrefDict.update({'database' : database, 'collection' : collection})

        searchFileName = database + "_" + collection + num
        self.webPrefs = Preferences(searchFileName, self.scrapPrefDict)
        
        self.webPrefs.saveConfig()

    '''
    loadSearch:
    loads previous searches
    '''
    def loadSearch(self, database, collection, num = ""):
        searchFileName = database + "_" + collection + num
        self.webPrefs = Preferences(searchFileName, self.scrapPrefDict)
        
        self.webPrefs.loadConfig()

    '''
    updateSearch:
    updates prefious searches with new info give in dict
    '''
    def updateSearch(self, dct):
        return 0

    '''
    multiSearch:
    '''
    def multiSearch(self, lst):
        return 0


class ScrapperDialog(QDialog):
    def __init__(self, db, parent = None):
        super(ScrapperDialog, self).__init__(parent)

        self.db = db
        self.dbName = db.mDbName
        self.tabName = db.mDbCollection
        self.loaded = False

        self.srchFile = self.dbName + "_" + self.tabName + "_search.json"
        self.loadSearch()

        self.setWindowTitle( self.tabName + " Look Up")
        self.setWindowModality(Qt.ApplicationModal)
        self.setGeometry(10,10,480,300)   

        self.initUI()
        self.move(center(self))


    def initUI(self):
        hBox = QHBoxLayout()
        vBox = QVBoxLayout()

        srchbtn = QPushButton('Search')#returns search settings for session use
        srchbtn.clicked.connect(self.search)
        
        cancelbtn = QPushButton('Cancel')
        cancelbtn.clicked.connect(self.reject)

        savebtn = QPushButton('Save')#returns search settings for session use and saves in a file for latter use
        savebtn.clicked.connect(self.saveSearch)

        hBtnBox = QHBoxLayout()
        hBtnBox.addWidget(srchbtn)
        hBtnBox.addWidget(savebtn)
        hBtnBox.addWidget(cancelbtn)

        btnWdgt = QWidget()
        btnWdgt.setLayout(hBtnBox)

        vBox.addWidget(self.tbv)
        vBox.addWidget(btnWdgt)

        leftLay = QWidget()
        leftLay.setLayout(vBox)
        
        tBar = QToolBar(self)
        tBar.setOrientation(Qt.Vertical)

        clrForm = QAction(QIcon("star.png"),"Clear Form",self)
        tBar.addAction(clrForm)

        hBox.addWidget(leftLay)
        hBox.addWidget(tBar)

        self.setLayout(hBox)

    def createTreeWidget(self, data = None):

        self.tbv = QTreeWidget()

        self.tbv.setHeaderLabels(['Keys', 'Values'])
        self.tbv.setColumnWidth(0,150)
        parent = QTreeWidgetItem(self.tbv)
       
        parent.setText(0, self.tabName)

        if data: #if there is saved search data
            pLChild = QTreeWidgetItem(parent)
            pLChild.setFlags(parent.flags() | Qt.ItemIsEditable)
            pLChild.setText(0,"page_link")
            pLChild.setText(1, data["page_link"])

            srchChild = QTreeWidgetItem(parent)
            srchChild.setText(0,"s")

            count = 1
            for key in self.db.mDbDocs[0]:
                if key == "_id":
                    continue
                child = QTreeWidgetItem(srchChild)
                child.setText(0, "s" + str(count) )
                
                count+=1

                fChild = QTreeWidgetItem(child)
                fChild.setText(0, "field_name") 
                fChild.setText(1,  data['s'][child.text(0)]["field_name"]) 

                dataChild = QTreeWidgetItem(child)
                dataChild.setFlags(child.flags() | Qt.ItemIsEditable)   
                dataChild.setText(0, "search") 
                dataChild.setText(1, str(data['s'][child.text(0)]["search"])) 

                tagChild = QTreeWidgetItem(child)
                tagChild.setFlags(child.flags() | Qt.ItemIsEditable)
                tagChild.setText(0, "tag") 
                tagChild.setText(1,  data['s'][child.text(0)]["tag"])  

                clsChild = QTreeWidgetItem(child)
                clsChild.setFlags(child.flags() | Qt.ItemIsEditable)
                clsChild.setText(0, "class")  
                clsChild.setText(1,  data['s'][child.text(0)]["class"]) 

                idChild = QTreeWidgetItem(child)
                idChild.setFlags(child.flags() | Qt.ItemIsEditable)
                idChild.setText(0, "id")
                idChild.setText(1,  data['s'][child.text(0)]["id"]) 

                rsltChild = QTreeWidgetItem(child)
                rsltChild.setFlags(child.flags() | Qt.ItemIsEditable)
                rsltChild.setText(0, "results") 
                rsltChild.setText(1, str(data['s'][child.text(0)]["results"])) 

                clnChild = QTreeWidgetItem(child)
                clnChild.setFlags(child.flags() | Qt.ItemIsEditable)
                clnChild.setText(0, "clean")
                clnChild.setText(1, str(data['s'][child.text(0)]["clean"])) 


        else: #if no saved search settings
            pLChild = QTreeWidgetItem(parent)
            pLChild.setFlags(parent.flags() | Qt.ItemIsEditable)
            pLChild.setText(0,"page_link")
            pLChild.setText(1, "link")

            srchChild = QTreeWidgetItem(parent)
            srchChild.setText(0,"s")

            count = 1
            for key in self.db.mDbDocs[0]:
                if key == "_id":
                    continue
                child = QTreeWidgetItem(srchChild)
                child.setText(0, "s" + str(count) )
                
                count+=1

                fChild = QTreeWidgetItem(child)
                fChild.setText(0, "field_name") 
                fChild.setText(1,  key) 

                dataChild = QTreeWidgetItem(child)
                dataChild.setFlags(child.flags() | Qt.ItemIsEditable)   
                dataChild.setText(0, "search") 
                dataChild.setText(1,  "False") 

                tagChild = QTreeWidgetItem(child)
                tagChild.setFlags(child.flags() | Qt.ItemIsEditable)
                tagChild.setText(0, "tag")  

                clsChild = QTreeWidgetItem(child)
                clsChild.setFlags(child.flags() | Qt.ItemIsEditable)
                clsChild.setText(0, "class")  

                idChild = QTreeWidgetItem(child)
                idChild.setFlags(child.flags() | Qt.ItemIsEditable)
                idChild.setText(0, "id")
                idChild.setText(1, "None")

                rsltChild = QTreeWidgetItem(child)
                rsltChild.setFlags(child.flags() | Qt.ItemIsEditable)
                rsltChild.setText(0, "results") 
                rsltChild.setText(1, "4")

                clnChild = QTreeWidgetItem(child)
                clnChild.setFlags(child.flags() | Qt.ItemIsEditable)
                clnChild.setText(0, "clean") 
                clnChild.setText(1, "False")

        self.tbv.expandToDepth(1)

        # to be able to decide on your own whether a particular item
        # can be edited, connect e.g. to itemDoubleClicked
        self.tbv.itemClicked.connect(self.checkEdit)

    # in your connected slot, you can implement any edit-or-not-logic
    # you want
    def checkEdit(self, item, column):
        # e.g. to allow editing only of column 1:
        if column == 1:
            self.tbv.editItem(item, column)

    '''
    formDict:
    creates dictionary of data from information in treewidget
    '''
    def formDict(self):
        root = self.tbv.invisibleRootItem()
        child_count = root.childCount()
        self.searchData = self.getChildData(root,child_count)[self.tabName]
        #pprint.pprint(self.searchData)

    '''
    getChildData
    recursivly iterates through tree widget items
    and puts each child into a dictionary then returns
    that dictionary to its parent
    '''
    def getChildData(self, pt, count):
        childData = {}
        for i in range(count):
            if pt.child(i).childCount() > 0:
                childData[pt.child(i).text(0)] = self.getChildData(pt.child(i),pt.child(i).childCount())

            else:
                childData[pt.child(i).text(0)] = getType(pt.child(i).text(1))
    
        return(childData)

    def search(self):
        self.formDict()
        self.accept()

    def saveSearch(self):
        self.formDict()
        
        with open(self.srchFile, 'w') as outfile:
            json.dump(self.searchData, outfile, indent=4, sort_keys=True)# save to file indent=4 & sort_keys=True make the file pretty

        self.accept()

    def loadSearch(self):
        if (os.path.isfile(self.srchFile) and
            os.stat(self.srchFile).st_size != 0): #check if file exists and is not empty
            
            with open(self.srchFile) as json_data_file:
                self.searchData = json.load(json_data_file)
                self.loaded = True
                #pprint.pprint( self.searchData)
                self.createTreeWidget(self.searchData)
        else:
            self.createTreeWidget()

'''
Dialog Box for getting keywords from user
'''
class ScarpSearchDialog(QDialog):   
    def __init__(self, pageLink, parent = None):
        super(ScarpSearchDialog, self).__init__(parent)
        self.keyLine = QLineEdit()
        self.fillers = []

        searchB = QPushButton("Search", self)
        searchB.clicked.connect(self.getLinkFill)
        
        cancelB = QPushButton("Cancel")
        cancelB.clicked.connect(self.reject)

        instruct = QLabel(pageLink + "\nPlease enter the keywords to replace the \"*\"" +
        "\nand form the search link. Use commas to seperate.")

        vBox = QVBoxLayout()
        formL = QFormLayout()

        formL.addRow(searchB,cancelB)

        widget = QWidget()
        widget.setLayout(formL)

        vBox.addWidget(instruct)
        vBox.addWidget(self.keyLine)
        vBox.addWidget(widget)

        self.setLayout(vBox)

    def getLinkFill(self):
        self.fillers = re.split(", |,|; |;", self.keyLine.text())
        #print(self.fillers)
        self.accept()

class DisplayResultsDialog(QDialog):   
    def __init__(self, data1, data2, parent = None):
        super(DisplayResultsDialog, self).__init__(parent)
        self.keyLine = QLineEdit()
        self.d1 = data1
        self.d2 = data2

        acceptB = QPushButton("Accept", self)
        acceptB.clicked.connect(self.returnData)
        
        cancelB = QPushButton("Cancel")
        cancelB.clicked.connect(self.reject)

        vBox = QVBoxLayout()
        
        '''
        for key in self.d1:
            edit = QLabel(self.d2['s'][key]['field_name'] + ":")
            vBox.addWidget(edit)
            resForm = QFormLayout()

            for i in range(len(self.d1[key])): 
                temp = QLabel(self.d1[key][i])
                resForm.addRow(temp,QCheckBox("ClickMe"))

            resWidget = QWidget()
            resWidget.setLayout(resForm)

            vBox.addWidget(resWidget)
        '''

        self.resForm = QFormLayout()

        for key in self.d1:
            edit = QLabel(self.d2['s'][key]['field_name'] + ":")
            self.resForm.addRow(edit)
            for i in range(len(self.d1[key])): 
                temp = QLabel(self.d1[key][i])
                self.resForm.addRow(temp,QCheckBox("ClickMe"))

        resWidget = QWidget()
        resWidget.setLayout(self.resForm)

        vBox.addWidget(resWidget)

        formL = QFormLayout()
        formL.addRow(acceptB,cancelB)

        widget = QWidget()
        widget.setLayout(formL)
        vBox.addWidget(widget)

        self.setLayout(vBox)

    def returnData(self):
        
        self.returnDict = {}
        key = None

        i=0
        while(i < self.resForm.count()):
            data = self.resForm.itemAt(i).widget().text()

            if data[-1] == ":":
                key = data[:-1]
                self.returnDict[key] = ""
            else:
                i+=1
                if self.resForm.itemAt(i).widget().isChecked():
                    if self.returnDict[key] == "":
                        self.returnDict[key] +=  data
                    else:
                        self.returnDict[key] += ("; " + data)
            i+=1
            
        self.accept()
